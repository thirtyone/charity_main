import axios from "axios";


var promise;


export default class PageContentService {



    store(data) {

        promise = axios.post('/api.admin/page-contents', data);
        return promise;
    }

    update(data) {
        promise = axios.put('/api.admin/page-contents/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = axios.get('/api.admin/page-contents', {params: data});
        return promise;
    }

    show(id) {
        promise = axios.get('/api.admin/page-contents/' + id);
        return promise;
    }

    delete(id) {
        promise = axios.delete('/api.admin/page-contents/' + id);
        return promise;
    }

}