import axios from "axios";


var promise;


export default class CarouselService {



    store(data) {

        promise = axios.post('/api.admin/carousels', data);
        return promise;
    }

    update(data) {
        promise = axios.put('/api.admin/carousels/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = axios.get('/api.admin/carousels', {params: data});
        return promise;
    }

    show(id) {
        promise = axios.get('/api.admin/carousels/' + id);
        return promise;
    }

    delete(id) {
        promise = axios.delete('/api.admin/carousels/' + id);
        return promise;
    }

}