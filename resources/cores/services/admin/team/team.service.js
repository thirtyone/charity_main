import axios from "axios";


var promise;


export default class TeamService {



    store(data) {

        promise = axios.post('/api.admin/teams', data);
        return promise;
    }

    update(data) {
        promise = axios.put('/api.admin/teams/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = axios.get('/api.admin/teams', {params: data});
        return promise;
    }

    show(id) {
        promise = axios.get('/api.admin/teams/' + id);
        return promise;
    }

    delete(id) {
        promise = axios.delete('/api.admin/teams/' + id);
        return promise;
    }

}