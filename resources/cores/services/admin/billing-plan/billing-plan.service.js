import axios from "axios";


var promise;


export default class BillingPlanService {



    store(data) {

        promise = axios.post('/api.admin/billing-plans', data);
        return promise;
    }

    update(data) {
        promise = axios.put('/api.admin/billing-plans/' + data.id, data);
        return promise;
    }

    list(data) {

        promise = axios.get('/api.admin/billing-plans', {params: data});
        return promise;
    }

    show(id) {
        promise = axios.get('/api.admin/billing-plans/' + id);
        return promise;
    }

    delete(id) {
        promise = axios.delete('/api.admin/billing-plans/' + id);
        return promise;
    }

}