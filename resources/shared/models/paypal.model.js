
export default class PayPalModel {

    billingPlans() {


        var response = {
            name: "",
            description: "",
            type: "INFINITE",
            state: "CREATED",
            payment_definition: {
                name: "",
                type: "REGULAR",
                frequency: "MONTH",
                frequency_interval: "12",
                cycles: "0",
                amount:
                    {
                        value: 0,
                        currency: "PHP"
                    }

            },
            charge_model: {
                type: "SHIPPING",
                amount:
                    {
                        value: 0,
                        currency: "PHP"
                    }

            },
            merchant_preference: {
                return_url: location.origin,
                cancel_url: location.origin,
                auto_bill_amount: 'yes',
                initial_fail_amount_action: 'CONTINUE',
                max_fail_attempts: '0',
                setup_fee:
                    {
                        value: 0,
                        currency: "PHP"
                    }


            }
        }


        return response;
    }
}