import Config from '../../configs/app.config';
import App from './app.component';
import LoginComponent from './components/auth/login.component';


//dashboard agent
import DashboardComponent from './components/dashboard/dashboard.component';




//profile
// import ProfileComponent from './components/profile/profile.component';
//
//
// function requireRole(to, from, next) {
//
//     const authRoles = JSON.parse(auth.admin_role.roles);
//
//
//     const found = authRoles.some(el => el == to.meta.role);
//
//     if (found) {
//         next();
//
//     }else{
//         next(prefix + '/my-profile');
//     }
//
//
//
//
// }

export default [

    {
        name: 'app',
        path: Config.admin_prefix + '/',
        component: App,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Dashboard'
        }
    },
    {
        name: 'login',
        path: Config.admin_prefix + '/login',
        component: LoginComponent,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Login'
        }
    },
    {
        name: 'dashboard',
        path: Config.admin_prefix + '/dashboard',
        component: DashboardComponent,
        // beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            // role: 'Dashboard',
            title: 'Dashboard'
        }
    },
    //accounts
    {
        name: 'admin-create',
        path: Config.admin_prefix + '/account/admins/create',
        component: require('./components/account/admin/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'Create Admin'
        }
    },
    {
        name: 'admin-update',
        path: Config.admin_prefix + '/account/admins/:id/edit',
        component: require('./components/account/admin/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'Update Admin'
        }

    },
    {
        name: 'admin-list',
        path: Config.admin_prefix + '/account/admins',
        component: require('./components/account/admin/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'List of Admins'
        }
    },
    //careers
    {
        name: 'career-create',
        path: Config.admin_prefix + '/careers/create',
        component: require('./components/career/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'careers',
                child: '',
            },
            role: "Careers",
            title: 'Create Career'
        }
    },
    {
        name: 'career-update',
        path: Config.admin_prefix + '/careers/:id/edit',
        component: require('./components/career/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'careers',
                child: '',
            },
            role: "Careers",
            title: 'Update Career'
        }

    },
    {
        name: 'career-list',
        path: Config.admin_prefix + '/careers',
        component: require('./components/career/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'careers',
                child: '',
            },
            role: "Careers",
            title: 'List of Career'
        }
    },
    //teams
    {
        name: 'team-create',
        path: Config.admin_prefix + '/teams/create',
        component: require('./components/team/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'teams',
                child: '',
            },
            role: "",
            title: 'Create Team'
        }
    },
    {
        name: 'team-update',
        path: Config.admin_prefix + '/teams/:id/edit',
        component: require('./components/team/form.component'),
        // beforeEnter : requireRole,team
        meta: {
            navigation: {
                parent: 'teams',
                child: '',
            },
            role: "",
            title: 'Update Team'
        }

    },
    {
        name: 'team-list',
        path: Config.admin_prefix + '/teams',
        component: require('./components/team/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'teams',
                child: '',
            },
            role: "",
            title: 'List of Teams'
        }
    },
    //carousel
    {
        name: 'carousel-create',
        path: Config.admin_prefix + '/carousels/create',
        component: require('./components/carousel/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'carousels',
                child: '',
            },
            role: "",
            title: 'Create Carousel'
        }
    },
    {
        name: 'carousel-update',
        path: Config.admin_prefix + '/carousels/:id/edit',
        component: require('./components/carousel/form.component'),
        // beforeEnter : requireRole,team
        meta: {
            navigation: {
                parent: 'carousels',
                child: '',
            },
            role: "",
            title: 'Update Carousel'
        }

    },
    {
        name: 'carousel-list',
        path: Config.admin_prefix + '/carousels',
        component: require('./components/carousel/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'carousels',
                child: '',
            },
            role: "",
            title: 'List of Carousels'
        }
    },

    //page-contents
    {
        name: 'page-content-create',
        path: Config.admin_prefix + '/page-contents/create',
        component: require('./components/page-content/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'page-contents',
                child: '',
            },
            role: "",
            title: 'Create Content'
        }
    },
    {
        name: 'page-content-update',
        path: Config.admin_prefix + '/page-contents/:id/edit',
        component: require('./components/page-content/form.component'),
        // beforeEnter : requireRole,team
        meta: {
            navigation: {
                parent: 'page-contents',
                child: '',
            },
            role: "",
            title: 'Update Content'
        }

    },
    {
        name: 'page-content-list',
        path: Config.admin_prefix + '/page-contents',
        component: require('./components/page-content/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'page-contents',
                child: '',
            },
            role: "",
            title: 'List of Contents'
        }
    },


    //billing
    {
        name: 'billing-plan-create',
        path: Config.admin_prefix + '/donate/billing-plans/create',
        component: require('./components/donate/billing-plan/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'donates',
                child: 'billing-plans',
            },
            role: "",
            title: 'Create Billing Plan'
        }
    },
    {
        name: 'billing-plan-update',
        path: Config.admin_prefix + '/donate/billing-plans/:id/edit',
        component: require('./components/donate/billing-plan/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'donates',
                child: 'billing-plans',
            },
            role: "",
            title: 'Update Billing Plan'
        }

    },
    {
        name: 'billing-plan-list',
        path: Config.admin_prefix + '/donate/billing-plans',
        component: require('./components/donate/billing-plan/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'donates',
                child: 'billing-plans',
            },
            role: "",
            title: 'List of Plans'
        }
    },

    {
        name: 'donation-list',
        path: Config.admin_prefix + '/donate/donations',
        component: require('./components/donate/donation/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'donates',
                child: 'donations',
            },
            role: "Donations",
            title: 'List of Donations'
        }
    },
    {
        name: 'inquiry-list',
        path: Config.admin_prefix + '/inquiries',
        component: require('./components/contact/list.component'),
        meta: {
            navigation: {
                parent: 'inquiries',
                child: '',
            },
            role: "inquiries",
            title: 'List of inquiries'
        }
    },

    //Mail Setting
    {
        name: 'mail-setting-create',
        path: Config.admin_prefix + '/mail-settings/create',
        component: require('./components/mail-setting/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'mail-setting',
                child: '',
            },
            role: "",
            title: 'Create Setting'
        }
    },
    {
        name: 'mail-setting-update',
        path: Config.admin_prefix + '/mail-settings/:id/edit',
        component: require('./components/mail-setting/form.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'mail-setting',
                child: '',
            },
            role: "",
            title: 'Update Setting'
        }

    },
    {
        name: 'mail-setting-list',
        path: Config.admin_prefix + '/mail-settings',
        component: require('./components/mail-setting/list.component'),
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'mail-setting',
                child: '',
            },
            role: "",
            title: 'List of Settings'
        }
    },
    {
        name: 'error-404',
        path: '*',
        component: require('./components/error/code-404.component'),
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            role: "",
            title: ''
        }

    }

];
