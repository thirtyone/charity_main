/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('../assets/bootstrap');
window.moment = require('moment');
window.Vue = require('vue');


import VueRouter from 'vue-router';

import AppRouter from './app.router';

import AppMixin from './app.mixin';



Vue.use(VueRouter);
Vue.mixin(AppMixin);


const router = new VueRouter(
    {
        mode: 'history',
        routes: AppRouter,
        scrollBehavior(to, from, savedPosition) {

            return {x: 0, y: 0}
        }
    });


const app = new Vue(Vue.util.extend({

    router,
    components: {},
    mounted: function () {

    },
    methods: {},
    watch: {
        $route(to, from) {



        }
    }


})).$mount('#app');
