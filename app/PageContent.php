<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    //

	protected $fillable = ["id","page_content_option_id","name","description","created_at","updated_at"];


	public function pageContentOption() {
	    return $this->belongsTo('App\PageContentOption');
	}
}
