<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model {
	//


	protected $fillable = ["id", "name", "description", "created_at", "updated_at"];


	public function hasManyImage() {
		return $this->morphMany('App\Image', 'table');
	}


	public function deleteRelatedData() {

		$this->hasManyImage()->delete();
	}

	public function delete() {
		parent::delete();
		$this->hasManyImage()->delete();
	}

}
