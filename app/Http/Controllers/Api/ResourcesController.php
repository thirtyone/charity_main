<?php

namespace App\Http\Controllers\Api;

use App\MailOption;

use App\PageContentOption;
use App\StatusOption;
use App\TeamPositionOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourcesController extends Controller {
	//

	/**
	 * @var StatusOption
	 */
	private $statusOption;
	private $mailOption;

	/**
	 * @var TeamPositionOption
	 */
	private $teamPositionOption;
	/**
	 * @var PageContentOption
	 */
	private $pageContentOption;


	/**
	 * ResourcesController constructor.
	 * @param StatusOption $statusOption
	 * @param MailOption $mailOption
	 */
	public function __construct(StatusOption $statusOption, MailOption $mailOption, TeamPositionOption $teamPositionOption, PageContentOption $pageContentOption) {

		$this->statusOption = $statusOption;
		$this->mailOption = $mailOption;

		$this->teamPositionOption = $teamPositionOption;
		$this->pageContentOption = $pageContentOption;
	}


	public function getStatusOptions() {
		$response = $this->statusOption->get();

		return response()->json($response,200);
	}


	public function getMailOptions(){

		$response = $this->mailOption->get();

		return response()->json($response,200);
	}


	public function getTeamPositionOptions() {
		$response = $this->teamPositionOption->get();

		return response()->json($response,200);
	}


	public function getPageContentOptions() {

		$response = $this->pageContentOption->get();

		return response()->json($response,200);
	}
}
