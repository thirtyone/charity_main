<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Response;
use File;
use Validator;

class UploadController extends Controller
{
	//

	protected $uploadPath;
	public function __construct(){

		$this->uploadPath = "images/uploads/";
	}


	public function uploadImage(Request $request) {



		if (!File::exists($this->uploadPath)) {
			File::makeDirectory($this->uploadPath);
		}


		$file = Input::file('file');

		$name = $file->getClientOriginalName();
		$fileName = time() . $name;

		$file->move($this->uploadPath, $fileName);

		$res = array(
			'file_name' => $fileName,
			'path' => "/".$this->uploadPath
		);

		return Response::json($res);

	}

}
