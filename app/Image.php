<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //


	protected $fillable = ["id","table_type","table_id","path","file_name","primary","created_at","updated_at"];

	public function table(){

		return $this->morphTo('table');
	}
}
