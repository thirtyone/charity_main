<?php namespace App\Repositories\PayPal;

use App\Repositories\PayPal\Contracts\PayPalInterface;
use Illuminate\Support\Facades\Config;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

class PayPalRepository implements PayPalInterface {


	private $api_context;
	/**
	 * @var Payer
	 */
	private $payer;
	/**
	 * @var Item
	 */
	private $item;

	private $items = [];
	/**
	 * @var ItemList
	 */
	private $itemList;
	/**
	 * @var Details
	 */
	private $details;
	/**
	 * @var Amount
	 */
	private $amount;
	/**
	 * @var Transaction
	 */
	private $transaction;
	/**
	 * @var RedirectUrls
	 */
	private $redirectUrls;
	/**
	 * @var Payment
	 */
	private $payment;
	/**
	 * @var PaymentExecution
	 */
	private $paymentExecution;
	/**
	 * @var ChargeModel
	 */
	private $chargeModel;
	/**
	 * @var Currency
	 */
	private $currency;
	/**
	 * @var MerchantPreferences
	 */
	private $merchantPreferences;
	/**
	 * @var PaymentDefinition
	 */
	private $paymentDefinition;
	/**
	 * @var Plan
	 */
	private $plan;
	/**
	 * @var Patch
	 */
	private $patch;
	/**
	 * @var PatchRequest
	 */
	private $patchRequest;
	/**
	 * @var PayPalModel
	 */
	private $palModel;

	public function __construct(Payer $payer, Item $item, ItemList $itemList, Details $details, Amount $amount, Transaction $transaction, RedirectUrls $redirectUrls, Payment $payment, PaymentExecution $paymentExecution, ChargeModel $chargeModel, Currency $currency, MerchantPreferences $merchantPreferences, PaymentDefinition $paymentDefinition, Plan $plan, Patch $patch, PatchRequest $patchRequest, PayPalModel $palModel) {

		$payPalConfig = Config::get('paypal');

		$this->api_context = new ApiContext(new OAuthTokenCredential($payPalConfig['checkout']['client_id'], $payPalConfig['checkout']['secret']));
		$this->api_context->setConfig($payPalConfig['checkout']['settings']);
		$this->payer = $payer;
		$this->item = $item;
		$this->itemList = $itemList;
		$this->details = $details;
		$this->amount = $amount;
		$this->transaction = $transaction;
		$this->redirectUrls = $redirectUrls;
		$this->payment = $payment;
		$this->paymentExecution = $paymentExecution;
		$this->chargeModel = $chargeModel;
		$this->currency = $currency;
		$this->merchantPreferences = $merchantPreferences;
		$this->paymentDefinition = $paymentDefinition;
		$this->plan = $plan;
		$this->patch = $patch;
		$this->patchRequest = $patchRequest;
		$this->palModel = $palModel;
	}

//	checkout
	public function checkout($data) {

		$this->payer = $this->payer->setPaymentMethod('paypal');

		foreach ($data['items'] as $key => $value) {
			$this->items[] = $this->item->setName($value['name'])
				->setCurrency($data['currency'])
				->setQuantity($value['quantity'])
				->setPrice($value['price']);
		}

		$this->itemList = $this->itemList->setItems($this->items);

		$this->amount = $this->amount->setCurrency($data['currency'])
			->setTotal($data['total'])
			->setDetails($this->details);

		$this->redirectUrls = $this->redirectUrls->setReturnUrl($data['return_url'])
			->setCancelUrl($data['cancel_url']);

		$this->transaction = $this->transaction->setAmount($this->amount)
			->setDescription($data['description'])
			->setInvoiceNumber(uniqid());


		$this->payment = $this->payment->setIntent($data['intent'])
			->setPayer($this->payer)
			->setRedirectUrls($this->redirectUrls)
			->setTransactions([$this->transaction]);

		try {
			$this->payment->create($this->api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			if (\Config::get('app.debug')) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;

				return $err_data = json_decode($ex->getData(), true);

			} else {

				die('Some error occur, sorry for inconvenient');
			}
		}


		$response = [];
		foreach ($this->payment->getLinks() as $link) {
			if ($link->getRel() == 'approval_url') {

				$response['redirect_url'] = $link->getHref();
				break;
			}
		}

		return $response;
	}

	/**
	 * @return PaymentExecution
	 */
	public function paymentExecution($data) {


		$response = [];
		$this->payment = $this->payment->get($data['paymentId'], $this->api_context);
		$this->paymentExecution = $this->paymentExecution->setPayerId($this->payment->getPayer()->getPayerInfo()->getPayerId());
		$this->payment = $this->payment->execute($this->paymentExecution, $this->api_context);

		$response['status'] = $this->payment->getState() == 'approved' ? true : false;

		return $response;
	}


//	end checkout

	public function createBillingPlan($data) {

		$this->plan = $this->plan->setName($data['name'])
			->setDescription($data["description"])
			->setType($data["type"]);
		// Set billing plan definitions

		$this->paymentDefinition = $this->paymentDefinition->setName($data['payment_definition']['name'])
			->setType($data['payment_definition']['type'])//REGULAR or REGULAR
			->setFrequency($data['payment_definition']['frequency'])//WEEK, DAY, YEAR, MONTH.
			->setFrequencyInterval($data['payment_definition']['frequency_interval'])//1 to 12
			->setCycles($data['payment_definition']['cycles'])//0 to 3
			->setAmount(new Currency($data['payment_definition']['amount']));


		$this->chargeModel = $this->chargeModel->setType($data['charge_model']['type'])//SHIPPING or TAX
		->setAmount(new Currency($data['charge_model']['amount']));


		$this->paymentDefinition->setChargeModels([$this->chargeModel]);


		$this->merchantPreferences = $this->merchantPreferences->setReturnUrl($data['merchant_preference']['return_url'])
			->setCancelUrl($data['merchant_preference']['cancel_url'])
			->setAutoBillAmount($data['merchant_preference']['auto_bill_amount'])
			->setInitialFailAmountAction($data['merchant_preference']['initial_fail_amount_action'])
			->setMaxFailAttempts($data['merchant_preference']['max_fail_attempts'])
			->setSetupFee(new Currency($data['merchant_preference']['setup_fee']));


		$this->plan->setPaymentDefinitions([$this->paymentDefinition])
			->setMerchantPreferences($this->merchantPreferences);


		try {
			$this->plan = $this->plan->create($this->api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			echo "Exception: " . $ex->getMessage() . PHP_EOL;

			return $err_data = json_decode($ex->getData(), true);


		}


		return $this->plan;
	}


}