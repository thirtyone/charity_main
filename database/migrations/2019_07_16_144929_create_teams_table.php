<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('teams', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');
			$table->integer('team_position_option_id')->unsigned();
			$table->foreign('team_position_option_id')
				->references('id')
				->on('team_position_options')
				->onDelete('cascade');
			$table->string('name');
			$table->longText('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('teams');
	}
}

