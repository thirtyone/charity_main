<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageContentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('page_contents', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');

			$table->integer('page_content_option_id')->unsigned();
			$table->foreign('page_content_option_id')
				->references('id')
				->on('page_content_options')
				->onDelete('cascade');

			$table->string('name');
			$table->longText('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('page_contents');
	}
}
