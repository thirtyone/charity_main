<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingPlansTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('billing_plans', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');
			$table->string('plan_id');
			$table->string('name');
			$table->longText('description');
			$table->string('state');
			$table->string('type');
			$table->longText('payment_definition');
			$table->longText('charge_model');
			$table->longText('merchant_preference');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('billing_plans');
	}
}
